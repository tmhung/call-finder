package com.rainbowdash.callfinder.domain;

public enum AudioExtension {
  MP3,
  WAV
}
