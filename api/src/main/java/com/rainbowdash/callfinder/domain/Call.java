package com.rainbowdash.callfinder.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Getter @Setter @NoArgsConstructor
@Entity @Table(name = "calls")
public class Call {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @Column
  private String name;

  @Column(name = "audio_data", nullable = false)
  @Lob
  @JsonIgnore
  private byte[] audioData;

  @Column
  private ZonedDateTime uploadedAt;

  @Column
  private AudioExtension audioExtension;


  public Call(String name, byte[] audioData) {
    this.name = name;
    this.audioData = audioData;
    this.uploadedAt = ZonedDateTime.now();
  }

}
