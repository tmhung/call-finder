package com.rainbowdash.callfinder.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter @Setter @NoArgsConstructor
@Entity @Table(name = "keywords")
public class Keyword {

    public Keyword(String word, int startIndex, int endIndex) {
        this.word = word;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)

    @JoinColumn(name="transcript_id", nullable=false)
    @JsonIgnore
    private Transcript transcript;

    @Column
    private String word;

    @Column private int startIndex;
    @Column private int endIndex;

    @Override
    public String toString() {
        return word;
    }
}
