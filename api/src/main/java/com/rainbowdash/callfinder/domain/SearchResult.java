package com.rainbowdash.callfinder.domain;

import lombok.Getter;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class SearchResult {

    private final List<SearchTermResult> searchTerms;
    private String transcript;
    private long id;

    public SearchResult(long id, String transcript, List<Keyword> keywords, Map<String, Set<String>> searchMap){
        this.id = id;
        this.transcript = transcript;
        searchTerms = keywords.stream()
                .sorted(Comparator.comparingInt(Keyword::getStartIndex))
                .map(keyword -> new SearchTermResult(keyword, searchMap))
                .collect(Collectors.toList());
    }

}
