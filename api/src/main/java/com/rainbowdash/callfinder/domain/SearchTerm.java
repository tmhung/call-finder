package com.rainbowdash.callfinder.domain;

import lombok.Getter;

@Getter
public class SearchTerm {
    private final String token;
    private final String word;

    public SearchTerm(String token, String word) {
        this.token = token;
        this.word = word;
    }
}
