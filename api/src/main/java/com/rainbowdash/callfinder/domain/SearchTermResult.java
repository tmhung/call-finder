package com.rainbowdash.callfinder.domain;

import lombok.Getter;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
public class SearchTermResult {

    private final String baseWord;
    private final int startIndex;
    private final int endIndex;
    private final String searchWords;

    public SearchTermResult(Keyword keyword, Map<String, Set<String>> searchMap){
        baseWord = keyword.getWord();
        startIndex = keyword.getStartIndex();
        endIndex = keyword.getEndIndex();
        searchWords = searchMap.getOrDefault(baseWord, Collections.emptySet())
                .stream()
                .collect(Collectors.joining(", "));
    }
}
