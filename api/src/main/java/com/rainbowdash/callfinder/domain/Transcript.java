package com.rainbowdash.callfinder.domain;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.List;

@Getter @Setter @NoArgsConstructor
@Entity @Table(name = "transcripts")
public class Transcript {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "call_id")
  private Call call;

  @Column
  @Lob
  private String content;

  @Column
  private ZonedDateTime transcribedAt;

  @OneToMany(mappedBy="transcript", cascade = CascadeType.ALL)
  public List<Keyword> keywords;

  public Transcript(Call call, String content, List<Keyword> keywords) {
    for(Keyword keyword : keywords){
      keyword.setTranscript(this);
    }
    this.call = call;
    this.content = content;
    this.transcribedAt = ZonedDateTime.now();
    this.keywords = keywords;
  }
}
