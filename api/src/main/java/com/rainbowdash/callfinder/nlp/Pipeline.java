package com.rainbowdash.callfinder.nlp;

import com.rainbowdash.callfinder.domain.Keyword;
import com.rainbowdash.callfinder.domain.SearchTerm;
import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class Pipeline {
  private final StanfordCoreNLP pipeline;

  public Pipeline() {
    this.pipeline = new StanfordCoreNLP(PropertiesUtils.asProperties(
      "annotators", "tokenize,ssplit,pos,lemma",
      "parse.model", "edu/stanford/nlp/models/srparser/englishSR.ser.gz", "tokenize.language", "en"
    ));
  }

  public List<Keyword> analyzeCall(String text) {
    Annotation document = new Annotation(text);
    pipeline.annotate(document);
    List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
    return sentences.stream()
      .flatMap(s -> s.get(CoreAnnotations.TokensAnnotation.class).stream())
      .map(token -> new Keyword(token.get(CoreAnnotations.LemmaAnnotation.class),
                    token.beginPosition(),
                    token.endPosition()))
      .collect(Collectors.toList());
  }

  public List<SearchTerm> analyzeQuery(String text) {
    Annotation document = new Annotation(text);
    pipeline.annotate(document);
    List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
    return sentences.stream()
            .flatMap(s -> s.get(CoreAnnotations.TokensAnnotation.class).stream())
            .map(token -> new SearchTerm(token.get(CoreAnnotations.LemmaAnnotation.class),
                    token.word())
            )
            .collect(Collectors.toList());
  }
}
