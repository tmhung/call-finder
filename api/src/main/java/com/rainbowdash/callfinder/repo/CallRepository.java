package com.rainbowdash.callfinder.repo;

import com.rainbowdash.callfinder.domain.Call;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface CallRepository extends JpaRepository<Call, Long>, QueryDslPredicateExecutor<Call> {
}
