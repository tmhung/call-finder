package com.rainbowdash.callfinder.repo;

import com.rainbowdash.callfinder.domain.Keyword;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface KeywordRepository  extends JpaRepository<Keyword, Long>, QueryDslPredicateExecutor<Keyword> {
}
