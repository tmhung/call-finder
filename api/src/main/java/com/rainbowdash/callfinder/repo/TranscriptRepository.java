package com.rainbowdash.callfinder.repo;

import com.rainbowdash.callfinder.domain.Transcript;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

public interface TranscriptRepository extends JpaRepository<Transcript, Long>, QueryDslPredicateExecutor<Transcript> {
}
