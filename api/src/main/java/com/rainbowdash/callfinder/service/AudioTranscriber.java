package com.rainbowdash.callfinder.service;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.api.StreamSpeechRecognizer;
import org.apache.commons.io.input.AutoCloseInputStream;
import org.springframework.stereotype.Component;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

@Component
public class AudioTranscriber {
  private Configuration configuration;
  public AudioTranscriber() {

    // TODO: double checks
    configuration = new Configuration();
    configuration.setAcousticModelPath("resource:/sphinx/en-us-adapt");
    configuration.setDictionaryPath("resource:/edu/cmu/sphinx/models/en-us/cmudict-en-us.dict");
    configuration.setLanguageModelPath("resource:/edu/cmu/sphinx/models/en-us/en-us.lm.bin");
  }

  public String transcribe(byte[] audioData) {
    try (InputStream audioStream = new AutoCloseInputStream(new ByteArrayInputStream(audioData))) {
      return transcribe(audioStream);
    } catch (IOException e) {
      throw new TranscribingException("Failed to load audio stream", e);
    }
  }

  public String transcribe(InputStream stream) throws IOException {
    StreamSpeechRecognizer recognizer = new StreamSpeechRecognizer(configuration);
    recognizer.startRecognition(stream);
    SpeechResult speechResult;
    String finalText = "";
    while ((speechResult = recognizer.getResult()) != null) {
      finalText += " " + speechResult.getHypothesis();
    }
    recognizer.stopRecognition();
    return finalText;
  }

}
