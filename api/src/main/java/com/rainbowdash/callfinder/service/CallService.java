package com.rainbowdash.callfinder.service;

import com.rainbowdash.callfinder.domain.Call;
import com.rainbowdash.callfinder.repo.CallRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class CallService {
  @Autowired
  private CallRepository callRepository;

  public Call uploadCall(String name, byte[] audioData) {
    return callRepository.save(new Call(name, audioData));
  }
}
