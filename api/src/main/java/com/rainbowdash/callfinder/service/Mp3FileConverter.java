package com.rainbowdash.callfinder.service;

import javazoom.jl.converter.Converter;
import javazoom.jl.decoder.JavaLayerException;
import org.springframework.stereotype.Component;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import java.io.*;

@Component
public class Mp3FileConverter {

    public File mp3ToWav(String sourceFile) throws JavaLayerException, IOException {
        Converter converter = new Converter();
        File wavFile = File.createTempFile("tmp-", ".wav");
        converter.convert(sourceFile, wavFile.getPath());
        return wavFile;
    }

    public  void changeBitrate(File source, File output) {
        AudioFormat format = new AudioFormat(16000, 16, 1, true, false);

        try {
            AudioInputStream in = AudioSystem.getAudioInputStream(source);
            AudioInputStream convert = AudioSystem.getAudioInputStream(format, in);
            AudioSystem.write(convert, AudioFileFormat.Type.WAVE, output);
        } catch (Exception e) {
            System.out.println(e);
        }
    }



    public void printFileType(String path) throws Exception {
        AudioFileFormat inputFileFormat = AudioSystem.getAudioFileFormat(new File(path));
        AudioInputStream mp3Stream = AudioSystem.getAudioInputStream(new File(path));

        AudioFormat audioFormat = mp3Stream.getFormat();

        System.out.println("File Format Type: " + inputFileFormat.getType());
        System.out.println("File Format String: " + inputFileFormat.toString());
        System.out.println("File length: " + inputFileFormat.getByteLength());
        System.out.println("Frame length: " + inputFileFormat.getFrameLength());
        System.out.println("Channels: " + audioFormat.getChannels());
        System.out.println("Encoding: " + audioFormat.getEncoding());
        System.out.println("Frame Rate: " + audioFormat.getFrameRate());
        System.out.println("Frame Size: " + audioFormat.getFrameSize());
        System.out.println("Sample Rate: " + audioFormat.getSampleRate());
        System.out.println("Sample size (bits): " + audioFormat.getSampleSizeInBits());
        System.out.println("Big endian: " + audioFormat.isBigEndian());
        System.out.println("Audio Format String: " + audioFormat.toString());
    }
}
