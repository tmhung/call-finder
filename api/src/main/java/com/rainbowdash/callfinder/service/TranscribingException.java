package com.rainbowdash.callfinder.service;

public class TranscribingException extends RuntimeException {
  public TranscribingException(Throwable cause) {
    super(cause);
  }

  public TranscribingException(String message, Throwable cause) {
    super(message, cause);
  }
}
