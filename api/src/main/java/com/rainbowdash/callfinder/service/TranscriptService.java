package com.rainbowdash.callfinder.service;

import com.rainbowdash.callfinder.domain.Call;
import com.rainbowdash.callfinder.domain.Keyword;
import com.rainbowdash.callfinder.domain.Transcript;
import com.rainbowdash.callfinder.nlp.Pipeline;
import com.rainbowdash.callfinder.repo.CallRepository;
import com.rainbowdash.callfinder.repo.TranscriptRepository;
import com.rainbowdash.callfinder.web.CallController;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileInputStream;
import java.util.List;

@Service
@Transactional
public class TranscriptService {
  private Logger logger = LoggerFactory.getLogger(CallController.class);
  private final static String MP3 = "mp3";

  @Autowired
  private TranscriptRepository transcriptRepository;

  @Autowired
  private AudioTranscriber audioTranscriber;

  @Autowired
  private CallRepository callRepository;

  @Autowired
  private Mp3FileConverter mp3FileConverter;
  @Autowired
  Pipeline nplPipeline;

  @Async
  public void transcriptAsync(long callId){
    logger.info("Transcription started");
    Transcript transcript = transcribe(callId);
    logger.info("Transcription done");
    logger.info(transcript.getContent());
    logger.info(transcript.keywords.toString());
  }

  public Transcript transcribe(long callId) {
    Call call = callRepository.getOne(callId);

    try {
      File originalAudioFile = File.createTempFile("origin-", "cf");
      FileUtils.writeByteArrayToFile(originalAudioFile, call.getAudioData());
      if (call.getName().toLowerCase().contains(MP3)) {
        originalAudioFile = mp3FileConverter.mp3ToWav(originalAudioFile.getPath());
      }

      File wavFile = File.createTempFile("origin-", "cf");
      mp3FileConverter.changeBitrate(originalAudioFile, wavFile);

      String text = audioTranscriber.transcribe(new FileInputStream(wavFile.getPath()));
      List<Keyword> keywords = nplPipeline.analyzeCall(text);
      return transcriptRepository.save(new Transcript(call, text, keywords));
    } catch (Exception e) {
      throw new TranscribingException(e);
    }
  }


}
