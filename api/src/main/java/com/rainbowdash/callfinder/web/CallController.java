package com.rainbowdash.callfinder.web;

import com.rainbowdash.callfinder.domain.Call;
import com.rainbowdash.callfinder.domain.Transcript;
import com.rainbowdash.callfinder.nlp.Pipeline;
import com.rainbowdash.callfinder.repo.TranscriptRepository;
import com.rainbowdash.callfinder.service.AudioTranscriber;
import com.rainbowdash.callfinder.service.CallService;
import com.rainbowdash.callfinder.service.TranscriptService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.beans.Transient;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RequestMapping("call")
@RestController
public class CallController {
  private Logger logger = LoggerFactory.getLogger(CallController.class);

  @Autowired
  private TranscriptRepository transcriptRepository;

  @Autowired
  private CallService callService;
  @Autowired
  private TranscriptService transcriptService;


  @RequestMapping(value = "uploadAudio", method = RequestMethod.POST)
  @Transactional
  public @ResponseBody Transcript uploadAudio(@RequestParam("files")MultipartFile[] files) throws IOException {
    Call call = callService.uploadCall(files[0].getOriginalFilename(), files[0].getBytes());
    Transcript transcript = transcriptService.transcribe(call.getId());
    logger.info("Transcript: [%s]", transcript.getContent());

    logger.info(transcript.keywords.toString());
    return transcript;
  }

  @RequestMapping
  public @ResponseBody Call upload(@RequestParam("files") MultipartFile[] files) throws IOException {
    Call call = callService.uploadCall(files[0].getOriginalFilename(), files[0].getBytes());
    transcriptService.transcriptAsync(call.getId());
    return call;
  }

  @RequestMapping(value="downloadAudio", method=RequestMethod.GET)
  public void getDownload(HttpServletResponse response, @RequestParam("id") long id) throws IOException {

    Transcript transcript = transcriptRepository.findOne(id);
    String callName = transcript.getCall().getName().toLowerCase();
    byte[] audioData = transcript.getCall().getAudioData();

    // Get your file stream from wherever.
    InputStream myStream = new ByteArrayInputStream(audioData);

    // Set the content type and attachment header.
    response.addHeader("Content-disposition", String.format("attachment;filename=%s", callName));
    String extension = callName.substring(callName.lastIndexOf(".") + 1);
    response.setContentType("audio/" + extension);

    // Copy the stream to the response's output stream.
    IOUtils.copy(myStream, response.getOutputStream());
    response.flushBuffer();
  }
}
