package com.rainbowdash.callfinder.web;

import com.querydsl.core.BooleanBuilder;
import com.rainbowdash.callfinder.domain.*;
import com.rainbowdash.callfinder.nlp.Pipeline;
import com.rainbowdash.callfinder.repo.KeywordRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RequestMapping("search")
@RestController
public class SearchController {

    @Autowired
    private Pipeline nlpPipeline;

    @Autowired
    private KeywordRepository keywordRepository;

    @RequestMapping
    public List<SearchResult> search(@RequestParam("query") String query) {
        List<SearchTerm> terms = nlpPipeline.analyzeQuery(query);

        if (terms.isEmpty()) return Collections.emptyList();

        Map<String, Set<String>> searchMap = terms.stream()
                .collect(Collectors.groupingBy(SearchTerm::getToken,
                        Collectors.mapping(SearchTerm::getWord, Collectors.toSet())));

        QKeyword keyword = QKeyword.keyword;

        BooleanBuilder predicate = new BooleanBuilder();
        for (SearchTerm term : terms) {
            predicate.or(keyword.word.eq(term.getToken()));
        }

        Iterable<Keyword> keywords = keywordRepository.findAll(predicate);

        Map<Transcript, List<Keyword>> countedTranscripts = StreamSupport.stream(keywords.spliterator(), false)
                .collect(
                        Collectors.groupingBy(
                                Keyword::getTranscript, Collectors.toList()
                        )
                );

        return countedTranscripts.entrySet().stream()
                .sorted(Comparator.comparingInt((Map.Entry<Transcript, List<Keyword>> e) -> e.getValue().size()).reversed())
                .map(entry -> {
                    Transcript t = entry.getKey();
                    return new SearchResult(t.getId(), t.getContent(), entry.getValue(), searchMap);
                })
                .collect(Collectors.toList());
    }
}
