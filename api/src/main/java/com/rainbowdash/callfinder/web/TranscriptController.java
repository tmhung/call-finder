package com.rainbowdash.callfinder.web;

import com.rainbowdash.callfinder.domain.Call;
import com.rainbowdash.callfinder.domain.Transcript;
import com.rainbowdash.callfinder.service.TranscriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RequestMapping("transcript")
@RestController
public class TranscriptController {
  @Autowired
  private TranscriptService transcriptService;

  @RequestMapping(method = RequestMethod.POST)
  public @ResponseBody Transcript transcribe(@RequestBody  Call call) {
    return transcriptService.transcribe(call.getId());
  }
}
