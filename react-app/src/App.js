import React, { Component } from 'react';
import './App.css';
import Uploader from './Components/Uploader';
import Search from './Components/Search';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1 className="App-title">Welcome to Call Report Search</h1>
        </header>
        <Uploader />
        <Search />
      </div>
    );
  }
}

export default App;
