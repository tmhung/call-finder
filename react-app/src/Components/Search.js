import React, { Component } from 'react';
import SearchResults from './SearchResults'

class Search extends Component {
  constructor() {
    super();
    this._handleChange = this._handleChange.bind(this);
    this._doSearch = this._doSearch.bind(this);
    this.state = {
      query: '',
      results: [],
      commence: false
    };
   }

  _doSearch() {
    this.setState({
      commence: true
    });

    fetch('/search?query=' + encodeURIComponent(this.state.query))
    .then(response => response.json())
    .then(results => {
        console.log(results);
        this.setState({results: results});
    })
  }

  _handleChange(e) {
    this.setState({query: e.target.value});
  }

  render () {
    return (
      <div className="SearchWrapper">
        <div className="Search">
          <input type="text" onChange={this._handleChange} className="SearchInput" placeholder="Search using keywords from call" /> <button className="SearchButton" onClick={this._doSearch}>Search</button>
          { this.state.commence && <SearchResults results={this.state.results} /> }
        </div>
      </div>
    )
  }
}

export default Search;
