import React, { Component } from 'react';

class SearchResults extends Component {

  constructor(props) {
    super(props);
    this._highlight = this._highlight.bind(this);
  }

  _highlight (transcript, searchTerms){
    let previous = 0;
    let parts = [];
    searchTerms.forEach((term) => {
      parts.push(
        <span className="normal" key={previous}>{transcript.substring(previous, term.startIndex)}</span>
      );
      parts.push(
        <span className="highlight" key={term.startIndex} title={term.baseWord + " : " + term.searchWords}>{transcript.substring(term.startIndex, term.endIndex)}</span>
      );
      previous = term.endIndex;
    });
    parts.push(
      <span className="normal" key={previous}>{transcript.substring(previous)}</span>
    );
    return parts;
  }

  _renderMessage() {
    return (
      <p className="NoResults">No results</p>
    )
  }

  render () {
    return (
      <div className="SearchTable">
        {this.props.results.length === 0 ? this._renderMessage() :
        <table>
          <thead>
            <tr>
              <th>Transcript</th>
              <th>Listen</th>
            </tr>
          </thead>
          <tbody>
            {
              this.props.results.map(({id, transcript, searchTerms}) => (
                <tr key={id}>
                  <td>
                    <div className="Transcript">
                    {this._highlight(transcript, searchTerms)}
                    </div>
                  </td>
                  <td>
                    <audio controls preload="auto">
                      <source src={"/call/downloadAudio?id=" + id} type="audio/wav" onChange={this._refresh}/>
                      <a href={"/call/downloadAudio?id=" + id}>Download</a>
                    </audio>
                  </td>
                </tr>
              ))
            }
          </tbody>
        </table>
      }
      </div>
    )
  }
}

export default SearchResults;
