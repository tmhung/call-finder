import React, { Component } from 'react';

class Uploader extends Component {

  constructor() {
    super();
    this._handleSubmit = this._handleSubmit.bind(this);
    this._getFile = this._getFile.bind(this);

    this.state = {
      disableUpload: true,
      file: null,
      message: ""
    }
  }

  _getFile(e) {
    e.preventDefault();

    this.setState({
      message: ""
    });

    if (e.target.files.length === 0) {
      this.setState({
        disableUpload: true,
        file: null
      })
      return;
    }

    let reader = new FileReader();
    let file = e.target.files[0];

    reader.onloadend = () => {
      this.setState({
        file: file,
        disableUpload: false
      });
    }

    reader.readAsDataURL(file);
  }

  _handleSubmit(e) {
    e.preventDefault();
    console.log(this.state.file)

    if (this.state.file === null) {
      return;
    }

    this.setState({
      disableUpload: true
    });

    const formData = new FormData();
    formData.append('files', this.state.file)

    fetch('/call' , {
      method: 'POST',
      headers: {
        'mime-type': 'multipart/form-data'
      },
      body: formData
    }).then(() => {
      this.setState({
        message: "File has been uploaded. It will take a few minutes for details of audio to be reflected in search."
      })
    })
  }

  render () {
    return (
      <div className="Uploader">
        <label>Upload an audio file</label>
        <form onSubmit={this._handleSubmit} encType="multipart/form-data">
            <input type="file" onChange={this._getFile}/>
            <input disabled={this.state.disableUpload} type="submit" value="Upload" />
            <p className="UploadMessage">{this.state.message}</p>
        </form>
      </div>
    )
  }
}

export default Uploader;
